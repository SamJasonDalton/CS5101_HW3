#include "Controller.h"
#include "Model.h"
#include "View.h"
#include <iostream>
using namespace std;

int main(){
  View view;
  Model model;
  Controller control(view, model);
  control.run();
}


