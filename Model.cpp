#include "Model.h"
#include <iostream>
using namespace std;

Model::Model(){
	reset();
	totalScore = 0;
}

Model::Model(int arr[], int maxIndex){
	for(int i = 0; i < maxIndex; i++){
		pegArray[i] = arr[i];
	}
	maxSize = maxIndex;
	currentScore = maxSize - 2;
	totalScore = 0;
}

Model::~Model(){}

bool Model::checkIfPegIsEmpty(int num){
	if(maxSize == 11){
		if (smallPegArray[num] == 0)
			return true;
		else 
			return false;
	}
	else if(maxSize == 16){
		if (pegArray[num] == 0)
			return true;
		else 
			return false;
	}
	else if(maxSize == 22){
		if (bigPegArray[num] == 0)
			return true;
		else 
			return false;
	}
	else
		return false;
}

bool Model::jumpPegAOverPegB(int a, int b){
	int check = -1;
	bool valid = false;
	if(!checkIfPegIsEmpty(a)){
		switch(a){
			case 1:
				if(!checkIfPegIsEmpty(b)){
					if(b == 2)
						check = 4;
					else if (b == 3)
						check = 6;
					else
						valid = false;
				}
				else
					valid = false;
				break;
				
			case 2:
				if(!checkIfPegIsEmpty(b)){
					if(b == 4)
						check = 7;
					else if (b == 5)
						check = 9;
					else
						valid = false;
				}
				else
					valid = false;
				break;
				
			case 3:
				if(!checkIfPegIsEmpty(b)){
					if(b == 5)
						check = 8;
					else if (b == 6)
						check = 10;
					else
						valid = false;
				}
				else
					valid = false;
				break;
				
			case 4:
				if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 2)
							check = 1;
						else if (b == 5)
							check = 6;
						else if (b == 7)
							check = 11;
						else if (b == 8)
							check = 13;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else if(maxSize == 11){
					if(!checkIfPegIsEmpty(b)){
						if(b == 2)
							check = 1;
						else if (b == 5)
							check = 6;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			case 5:
				if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 8)
							check = 12;
						else if (b == 9)
							check = 14;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else if (maxSize == 11){
						valid = false;
				}
				break;
			
			case 6:
				if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 3)
							check = 1;
						else if (b == 5)
							check = 4;
						else if (b == 9)
							check = 13;
						else if (b == 10)
							check = 15;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else if(maxSize == 11){
					if(!checkIfPegIsEmpty(b)){
						if(b == 3)
							check = 1;
						else if (b == 5)
							check = 4;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 7:
				if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 4)
							check = 2;
						else if (b == 8)
							check = 9;
						else if (b == 11)
							check = 16;
						else if (b == 12)
							check = 18;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else{
					if(!checkIfPegIsEmpty(b)){
						if(b == 4)
							check = 2;
						else if (b == 8)
							check = 9;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 8:
				if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 5)
							check = 3;
						else if (b == 9)
							check = 10;
						else if (b == 12)
							check = 17;
						else if (b == 13)
							check = 19;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else{
					if(!checkIfPegIsEmpty(b)){
						if(b == 5)
							check = 3;
						else if (b == 9)
							check = 10;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 9:
				if(maxSize == 22){
						if(!checkIfPegIsEmpty(b)){
							if(b == 5)
								check = 2;
							else if (b == 8)
								check = 7;
							else if (b == 14)
								check = 20;
							else if ( b == 13)
								check = 18;
							else
								valid = false;
						}
						else
							valid = false;
				}
				else{
					if(!checkIfPegIsEmpty(b)){
						if(b == 5)
							check = 2;
						else if (b == 8)
							check = 7;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			case 10:
				if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 6)
							check = 3;
						else if (b == 9)
							check = 8;
						else if (b == 14)
							check = 19;
						else if ( b == 15)
							check = 21;
						else
							valid = false;
					}
					else
						valid = false;
				}
				else{
					if(!checkIfPegIsEmpty(b)){
						if(b == 6)
							check = 3;
						else if (b == 9)
							check = 8;
						else
							valid = false;
					}
					else
						valid = false;
				}
				
				break;
			
			case 11:
				if(maxSize == 11)
					break;
				else if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 7)
							check = 4;
						else if (b == 12)
							check = 13;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 12:
				if(maxSize == 11)
					break;
				else if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 8)
							check = 5;
						else if (b == 13)
							check = 14;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			case 13:
				if(maxSize == 11)
					break;
				else if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 8)
							check = 4;
						else if (b == 9)
							check = 6;
						else if (b == 12)
							check = 11;
						else if (b == 14)
							check = 15;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			case 14:
				if(maxSize == 11)
					break;
				else if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 9)
							check = 5;
						else if (b == 13)
							check = 12;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			case 15:
				if(maxSize == 11)
					break;
				else if(maxSize == 16 || maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 10)
							check = 6;
						else if (b == 14)
							check = 13;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 16:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 11)
							check = 7;
						else if (b == 17)
							check = 18;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 17:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 12)
							check = 8;
						else if (b == 18)
							check = 19;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 18:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 12)
							check = 7;
						else if (b == 13)
							check = 9;
						else if (b == 17)
							check = 16;
						else if (b == 19)
							check = 20;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 19:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 13)
							check = 8;
						else if (b == 14)
							check = 10;
						else if (b == 18)
							check = 17;
						else if (b == 20)
							check = 21;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 20:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 14)
							check = 9;
						else if (b == 19)
							check = 18;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
				
			case 21:
				if(maxSize == 11 || maxSize == 16)
					break;
				else if(maxSize == 22){
					if(!checkIfPegIsEmpty(b)){
						if(b == 15)
							check = 10;
						else if (b == 20)
							check = 19;
						else
							valid = false;
					}
					else
						valid = false;
				}
				break;
			
			default:
				valid = false;
				break;
		}
		if(checkIfPegIsEmpty(check) && check > 0 && check < maxSize)
			valid = true;
		else
			valid = false;
	}
	else 
		valid = false;

	if(valid){
		if(maxSize == 11){
			smallPegArray[a] = 0;
			smallPegArray[b] = 0;
			smallPegArray[check] = 1;
		}
		else if(maxSize == 16){
			pegArray[a] = 0;
			pegArray[b] = 0;
			pegArray[check] = 1;
		}
		else if(maxSize == 22){
			bigPegArray[a] = 0;
			bigPegArray[b] = 0;
			bigPegArray[check] = 1;
		}
		currentScore--;
		totalScore--;
		return true;
	}
	else
		return false;
}

bool Model::checkIfPegIsTouching(int num){
	switch(num){
		case 1:
			if(!checkIfPegIsEmpty(2) || !checkIfPegIsEmpty(3))
				return true;
			break;
		case 2:
			if(!checkIfPegIsEmpty(1) || !checkIfPegIsEmpty(3) || !checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(5))
				return true;
			break;
		case 3:
			if(!checkIfPegIsEmpty(1) || !checkIfPegIsEmpty(2) || !checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(6))
				return true;
			break;
		case 4:
			if(!checkIfPegIsEmpty(2) || !checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(7) || !checkIfPegIsEmpty(8))
				return true;
			break;
		case 5:
			if(!checkIfPegIsEmpty(2) || !checkIfPegIsEmpty(3) || !checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(6) || !checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(9))
				return true;
			break;
		case 6:
			if(!checkIfPegIsEmpty(3) || !checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(10))
				return true;
			break;
		case 7:
			if(!checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(11) || !checkIfPegIsEmpty(12))
				return true;
			break;
		case 8:
			if(!checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(7) || !checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(12) || !checkIfPegIsEmpty(13))
				return true;
			break;
		case 9:
			if(!checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(6) || !checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(10) || !checkIfPegIsEmpty(13) || !checkIfPegIsEmpty(14))
				return true;
			break;
		case 10:
			if(!checkIfPegIsEmpty(6) || !checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(14) || !checkIfPegIsEmpty(15))
				return true;
			break;
		case 11:
			if(!checkIfPegIsEmpty(7) || !checkIfPegIsEmpty(12))
				return true;
			break;
		case 12:
			if(!checkIfPegIsEmpty(7) || !checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(11) || !checkIfPegIsEmpty(13))
				return true;
			break;
		case 13:
			if(!checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(12) || !checkIfPegIsEmpty(14))
				return true;
			break;
		case 14:
			if(!checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(10) || !checkIfPegIsEmpty(13) || !checkIfPegIsEmpty(15))
				return true;
			break;
		case 15:
			if(!checkIfPegIsEmpty(10) || !checkIfPegIsEmpty(14))
				return true;
			break;
		default:
			return false;
			break;
	}
	return false;
}

bool Model::checkIfNoMoves(){
	for(int i = 1; i < 16; i++){
		if(!checkIfPegIsEmpty(i)){
			switch(i){
				case 2:
					if(!checkIfPegIsEmpty(1) || !checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(5))
						return false;
					break;
				case 3:
					if(!checkIfPegIsEmpty(1) || !checkIfPegIsEmpty(5) || !checkIfPegIsEmpty(6))
						return false;
					break;
				case 7:
					if(!checkIfPegIsEmpty(4) || !checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(11))
						return false;
					break;
				case 10:
					if(!checkIfPegIsEmpty(6) || !checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(15))
						return false;
					break;
				case 12:
					if(!checkIfPegIsEmpty(8) || !checkIfPegIsEmpty(11) || !checkIfPegIsEmpty(13))
						return false;
					break;
				case 14:
					if(!checkIfPegIsEmpty(9) || !checkIfPegIsEmpty(13) || !checkIfPegIsEmpty(15))
						return false;
					break;
				default:
					if(checkIfPegIsTouching(i))
						return false;
					break;
			}
		}
	}
	return true;
}

int Model::getCurrentScore(){
	return currentScore;
}

int Model::getTotalScore(){
	return totalScore;
}

int* Model::getPegArray(int size){
	if(size == 0)
		return smallPegArray;
	else if(size == 2)
		return bigPegArray;
	else
		return pegArray;
}

void Model::reset(){
	maxSize = 16;
	for(int i = 1; i < maxSize; i++){
		pegArray[i] = 1;
	}
	pegArray[1] = 0; //Initial empty space
	currentScore = maxSize - 2;
	totalScore += currentScore;
}

void Model::resize(int size){
	if(size == 0){
		maxSize = 11;
		for(int i = 1; i < maxSize; i++){
			smallPegArray[i] = 1;
		}
		smallPegArray[1] = 0; //Initial empty space
		currentScore = maxSize - 2;
		totalScore += currentScore;
	}
	else if(size == 2){
		maxSize = 22;
		for(int i = 1; i < maxSize; i++){
			bigPegArray[i] = 1;
		}
		bigPegArray[1] = 0; //Initial empty space
		currentScore = maxSize - 2;
		totalScore += currentScore;
	}
	else
		reset();
}









