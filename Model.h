#ifndef MODEL_H
#define MODEL_H
class Model{
	public:
		Model();
		Model(int arr[], int maxIndex);
		~Model();
		bool checkIfPegIsEmpty(int num);
		bool jumpPegAOverPegB(int a, int b);
		bool checkIfPegIsTouching(int num);
		bool checkIfNoMoves();
		int getCurrentScore();
		int getTotalScore();
		int* getPegArray(int size);
		void reset();
		void resize(int size);
	private:
		int pegArray[16], smallPegArray[11], bigPegArray[22];
		int maxSize, currentScore, totalScore;
};

#endif