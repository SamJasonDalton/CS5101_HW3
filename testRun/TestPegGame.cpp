#include <iostream>
#include <unistd.h>
#include "TestPegGame.h"
#include "../Model.h"

using namespace std;

void TestPegGame::setUp(){
}

void TestPegGame::tearDown(){
}

void TestPegGame::testCheckIfPegIsEmpty(){
    Model m;
	bool expected = true;
	bool actual = m.checkIfPegIsEmpty(1);
	CPPUNIT_ASSERT(expected == actual);
}

void TestPegGame::testJumpPegAOverPegB(){
	Model m;
	int p1 = 6;
	int p2 = 3;
	int checkPeg = 1;
	bool expected = true;
	bool actual = m.jumpPegAOverPegB(p1, p2);
	CPPUNIT_ASSERT(expected == actual);
	CPPUNIT_ASSERT(m.checkIfPegIsEmpty(p1) == true);
	CPPUNIT_ASSERT(m.checkIfPegIsEmpty(p2) == true);
	CPPUNIT_ASSERT(m.checkIfPegIsEmpty(checkPeg) == false);
}

void TestPegGame::testCheckIfPegIsTouching(){
    Model m;
	bool expected = true;
	bool actual1 = m.checkIfPegIsTouching(1);
	CPPUNIT_ASSERT(expected == actual1);
	bool actual2 = m.checkIfPegIsTouching(2);
	CPPUNIT_ASSERT(expected == actual2);
	bool actual3 = m.checkIfPegIsTouching(3);
	CPPUNIT_ASSERT(expected == actual3);
	bool actual4 = m.checkIfPegIsTouching(4);
	CPPUNIT_ASSERT(expected == actual4);
	bool actual5 = m.checkIfPegIsTouching(5);
	CPPUNIT_ASSERT(expected == actual5);
	bool actual6 = m.checkIfPegIsTouching(6);
	CPPUNIT_ASSERT(expected == actual6);
	bool actual7 = m.checkIfPegIsTouching(7);
	CPPUNIT_ASSERT(expected == actual7);
	bool actual8 = m.checkIfPegIsTouching(8);
	CPPUNIT_ASSERT(expected == actual8);
	bool actual9 = m.checkIfPegIsTouching(9);
	CPPUNIT_ASSERT(expected == actual9);
	bool actual10 = m.checkIfPegIsTouching(10);
	CPPUNIT_ASSERT(expected == actual10);
	bool actual11 = m.checkIfPegIsTouching(11);
	CPPUNIT_ASSERT(expected == actual11);
	bool actual12 = m.checkIfPegIsTouching(12);
	CPPUNIT_ASSERT(expected == actual12);
	bool actual13 = m.checkIfPegIsTouching(13);
	CPPUNIT_ASSERT(expected == actual13);
	bool actual14 = m.checkIfPegIsTouching(14);
	CPPUNIT_ASSERT(expected == actual14);
	bool actual15 = m.checkIfPegIsTouching(15);
	CPPUNIT_ASSERT(expected == actual15);
}

void TestPegGame::testCheckIfNoMoves(){
	int arr[] = {0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0};
	Model m(arr, 16);
	bool expected = true;
	bool actual = m.checkIfNoMoves();
	CPPUNIT_ASSERT(expected == actual);
	
	int arr2[] = {0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0};
	Model m2(arr2, 16);
	expected = false;
	bool actual2 = m2.checkIfNoMoves();
	CPPUNIT_ASSERT(expected == actual2);
}

void TestPegGame::testGetCurrentScore(){
	Model m;
	int expected = 12;
	m.jumpPegAOverPegB(6, 3);
	m.jumpPegAOverPegB(13, 9);
	int actual = m.getCurrentScore();
	CPPUNIT_ASSERT(expected == actual);
}

void TestPegGame::testGetTotalScore(){
	Model m;
	m.reset();
	m.jumpPegAOverPegB(6, 3);
	m.jumpPegAOverPegB(13, 9);
	m.reset();
	int expected = 26;
	int actual = m.getTotalScore();
	CPPUNIT_ASSERT(expected == actual);
	
}






