#ifndef TestPegGame_h
#define TestPegGame_h

#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

class TestPegGame : public TestFixture
{
    CPPUNIT_TEST_SUITE(TestPegGame);
    CPPUNIT_TEST(testCheckIfPegIsEmpty);
	CPPUNIT_TEST(testJumpPegAOverPegB);
	CPPUNIT_TEST(testCheckIfPegIsTouching);
	CPPUNIT_TEST(testCheckIfNoMoves);
	CPPUNIT_TEST(testGetCurrentScore);
	CPPUNIT_TEST(testGetTotalScore);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp(void);
    void tearDown(void);

protected:
    void testCheckIfPegIsEmpty();
	void testJumpPegAOverPegB();
	void testCheckIfPegIsTouching();
	void testCheckIfNoMoves();
	void testGetCurrentScore();
	void testGetTotalScore();

private:

};

#endif
