#include "View.h"
#include <stdlib.h>

View::View(){
	totalScore = 0;
	boardSize = 1;
}


View::~View(){}

void View::getMove(){
	bool valid = false;
	while (!valid){
		cout << "Enter the number of a peg.\nPeg 1: ";
		cin >> p1str;
		if(p1str == "quit")
			exit(0);
		else if(p1str == "help" || p1str == "solution" || p1str == "scoreboard" || p1str == "sizeup" || p1str == "sizedown"){
			p2str = "";
		}
		else{
			cout << "Enter the number of the peg you want to jump.\nPeg 2: ";
			cin >> p2str;
		}
		if(p2str == "quit")
			exit(0);
		valid = validInput();
	}
}

int View::getP1(){
	return p1;
}

int View::getP2(){
	return p2;
}

bool View::validInput(){
	const char* cstr = p1str.c_str();
	p1 = atoi(cstr);
	cstr = p2str.c_str();
	p2 = atoi(cstr);
	
	if(((p1<=10 && p1>=1) && (p2<=10 && p2>=1)) && boardSize == 0){
		return true;
	}
	else if(((p1<=15 && p1>=1) && (p2<=15 && p2>=1)) && boardSize == 1){
		return true;
	}
	else if(((p1<=21 && p1>=1) && (p2<=21 && p2>=1)) && boardSize == 2){
		return true;
	}
	else if(p1str == "help" || p2str == "help"){
		showHelp();
		displayBoard(pegArray, boardSize);
	}
	else if(p1str == "solution" || p2str == "solution"){
		showSolution();
		displayBoard(pegArray, boardSize);
	}
	else if(p1str == "scoreboard" || p2str == "scoreboard"){
		showTotalScore();
		displayBoard(pegArray, boardSize);
	}
	else if(p1str == "sizeup" || p2str == "sizeup"){
		boardSize++;
		if(boardSize > 2)
			boardSize = 2;
		return true;
	}
	else if(p1str == "sizedown" || p2str == "sizedown"){
		boardSize--;
		if(boardSize < 0)
			boardSize = 0;
		return true;
	}
	else{
		cout << "\nInput invalid. Please enter the number of two pegs.\n" << endl;
		return false;
	}
}

void View::invalidMove(){
	cout << "\nYou cannot make that move." << endl << endl;
}

void View::displayMenu(){
	cout << "Press help at any time to find a list of possible commands." << endl << endl;
	cout << "Please type the number for the game you would like to play." << endl;
	cout << "1 - Peg Jump" << endl;
	cout << "2 - Chess" << endl;
	cout << "3 - Jumanji" << endl;
	cout << "4 - Half Life 3" << endl;
}

int View::getGameChoice(){
	bool valid = false;
	const char* cstr;
	
	while(!valid){
		displayMenu();
		cout << "Game selection : ";
		cin >> gamestr;
		cstr = gamestr.c_str();
		gameNum = atoi(cstr);
		if(gamestr == "quit")
			exit(0);
		else if(gamestr == "help")
			showHelp();
		else if(gameNum > 0 && gameNum < 5)
			return gameNum;
		else
			cout << "\nYou must enter the number of a game on the list." << endl;
	}
}

void View::notAvailable(){
	cout << "\nGame not available." << endl;
}

void View::showGameOver(int score){
	if(score == 1)
		cout << "CONGRATULATIONS!" << endl << endl;
	else
		cout << "GAME OVER" << endl << endl;
	cout << "Your final score is: " << score << endl;
}

bool View::getPlayAgain(){
	bool valid = false;
	const char* cstr;
	string again;
	while(!valid){
		cout << "\nWould you like to play again?\n(y/n): " << endl;
		cin >> again;
		if(again == "y"){
			valid = true;
			return true;
		}
		else if(again == "n"){
			cout << "\nThanks for playing!" << endl << endl;
			valid = true;
		}
		else{
			cout << "Invalid input." << endl;
			valid = false;
		}
	}
	return false;
}

void View::showHelp(){
	cout << "\nThis is a list of every available command in this program." << endl;
	cout << "help - Displays the help menu." << endl;
	cout << "quit - Terminates the program." << endl;
	cout << "\n------Peg Game------" << endl;
	cout << "solution - Shows a possible solution to the standard peg game." << endl;
	cout << "scoreboard - Displays the cumulative score across all games." << endl;
	cout << "sizeup - Extends the game board size by one row and restarts the game." << endl;
	cout << "sizedown - Reduces the game board size by one row and restarts the game." << endl;
	cout << "\nNote: sizeup and sizedown will reset the game." << endl;
	cout << "--------------------" << endl << endl;
}

void View::displayBoard(int arr[], int size){
	pegArray = arr;
	if(size == 0){
		cout << endl;
		cout << "          ^             "	<< "          ^" 			<< endl;
		cout << "         /1\\            "	<< "         /"<<pegArray[1]<<"\\" 			<< endl;
		cout << "        /   \\           "	<< "        /   \\"			<< endl;
		cout << "       /2   3\\          " << "       /"<<pegArray[2]<<"   "<<pegArray[3]<<"\\"		<< endl;  
		cout << "      /       \\         " << "      /       \\"		<< endl;
		cout << "     /4   5   6\\        "	<< "     /"<<pegArray[4]<<"   "<<pegArray[5]<<"   "<<pegArray[6]<<"\\"		<< endl;
		cout << "    /           \\       "	<< "    /           \\"		<< endl;
		cout << "   /7   8   9  10\\      "	<< "   /"<<pegArray[7]<<"   "<<pegArray[8]<<"   "<<pegArray[9]<<"   "<<pegArray[10]<<"\\"	<< endl;
		cout << "  /_______________\\     "	<< "  /_______________\\"	<< endl << endl;
		cout << "      Reference         "	<< "    Current Board" << endl << endl;
	}
	else if(size == 1){
		cout << endl;
		cout << "          ^             "	<< "          ^" 			<< endl;
		cout << "         /1\\            "	<< "         /"<<pegArray[1]<<"\\" 			<< endl;
		cout << "        /   \\           "	<< "        /   \\"			<< endl;
		cout << "       /2   3\\          " << "       /"<<pegArray[2]<<"   "<<pegArray[3]<<"\\"		<< endl;  
		cout << "      /       \\         " << "      /       \\"		<< endl;
		cout << "     /4   5   6\\        "	<< "     /"<<pegArray[4]<<"   "<<pegArray[5]<<"   "<<pegArray[6]<<"\\"		<< endl;
		cout << "    /           \\       "	<< "    /           \\"		<< endl;
		cout << "   /7   8   9  10\\      "	<< "   /"<<pegArray[7]<<"   "<<pegArray[8]<<"   "<<pegArray[9]<<"   "<<pegArray[10]<<"\\"	<< endl;
		cout << "  /               \\     "	<< "  /               \\"	<< endl;
		cout << " /11  12  13  14 15\\    "	<< " /"<<pegArray[11]<<"   "<<pegArray[12]<<"   "<<pegArray[13]<<"   "<<pegArray[14]<<"   "<<pegArray[15]<<"\\"	<< endl;
		cout << "/___________________\\   "	<< "/___________________\\"	<< endl << endl;
		cout << "      Reference         "	<< "    Current Board" << endl << endl;
	}
	else if(size == 2){
		cout << endl;
		cout << "            ^             "	<< "            ^" 			<< endl;
		cout << "           /1\\            "	<< "           /"<<pegArray[1]<<"\\" 			<< endl;
		cout << "          /   \\           "	<< "          /   \\"			<< endl;
		cout << "         /2   3\\          " 	<< "         /"<<pegArray[2]<<"   "<<pegArray[3]<<"\\"		<< endl;  
		cout << "        /       \\         " 	<< "        /       \\"		<< endl;
		cout << "       /4   5   6\\        "	<< "       /"<<pegArray[4]<<"   "<<pegArray[5]<<"   "<<pegArray[6]<<"\\"		<< endl;
		cout << "      /           \\       "	<< "      /           \\"		<< endl;
		cout << "     /7   8   9  10\\      "	<< "     /"<<pegArray[7]<<"   "<<pegArray[8]<<"   "<<pegArray[9]<<"   "<<pegArray[10]<<"\\"	<< endl;
		cout << "    /               \\     "	<< "    /               \\"	<< endl;
		cout << "   /11  12  13  14 15\\    "	<< "   /"<<pegArray[11]<<"   "<<pegArray[12]<<"   "<<pegArray[13]<<"   "<<pegArray[14]<<"   "<<pegArray[15]<<"\\"	<< endl;
		cout << "  /                   \\   "	<< "  /                   \\"	<< endl;
		cout << " /16  17  18  19  20 21\\  "	<< " /"<<pegArray[16]<<"   "<<pegArray[17]<<"   "<<pegArray[18]<<"   "<<pegArray[19]<<"   "<<pegArray[20]<<"   "<<pegArray[21]<<"\\  " << endl;
		cout << "/_______________________\\ "	<< "/_______________________\\ " << endl << endl;
		cout << "      Reference         "	<< "    Current Board" << endl << endl;
	}
}

void View::showSolution(){
	cout << endl;
	cout << "          ^             " 	<< "          ^             "	<< "          ^             "	<< endl;
	cout << "         /1\\            "	<< "         /0\\            "	<< "         /1\\            "	<< endl;
	cout << "        /   \\           "	<< "        /   \\           "	<< "        /   \\           "	<< endl;
    cout << "       /2   3\\          " << "       /1   1\\          "	<< "       /1   0\\          "	<< endl;
    cout << "      /       \\         " << "      /       \\         "	<< "      /       \\         "	<< endl;
	cout << "     /4   5   6\\        "	<< "     /1   1   1\\        "	<< "     /1   1   0\\        "	<< endl;
	cout << "    /           \\       "	<< "    /           \\       "	<< "    /           \\       "	<< endl;
	cout << "   /7   8   9  10\\      "	<< "   /1   1   1   1\\      "	<< "   /1   1   1   1\\      "	<< endl;
	cout << "  /               \\     "	<< "  /               \\     "	<< "  /               \\     "	<< endl;
	cout << " /11  12  13  14 15\\    "	<< " /1   1   1   1   1\\    "	<< " /1   1   1   1   1\\    "	<< endl;
	cout << "/___________________\\   "	<< "/___________________\\   "	<< "/___________________\\   "	<< endl;
	cout << "      Reference         "	<< "  Starting Position     " 	<< "      6 jumps 3         "	<< endl;
	cout << endl;
	cout << "          ^             " 	<< "          ^             "	<< "          ^             "	<< endl;
	cout << "         /1\\            "	<< "         /1\\            "	<< "         /1\\            "	<< endl;
	cout << "        /   \\           "	<< "        /   \\           "	<< "        /   \\           "	<< endl;
    cout << "       /1   0\\          " << "       /1   0\\          "	<< "       /1   0\\          "	<< endl;
    cout << "      /       \\         " << "      /       \\         "	<< "      /       \\         "	<< endl;
	cout << "     /1   1   1\\        "	<< "     /1   1   1\\        "	<< "     /1   1   1\\        "	<< endl;
	cout << "    /           \\       "	<< "    /           \\       "	<< "    /           \\       "	<< endl;
	cout << "   /1   1   0   1\\      "	<< "   /0   0   1   1\\      "	<< "   /0   0   1   1\\      "	<< endl;
	cout << "  /               \\     "	<< "  /               \\     "	<< "  /               \\     "	<< endl;
	cout << " /1   1   0   1   1\\    "	<< " /1   1   0   1   1\\    "	<< " /0   0   1   1   1\\    "	<< endl;
	cout << "/___________________\\   "	<< "/___________________\\   "	<< "/___________________\\   "	<< endl;
	cout << "      13 jumps 9        "	<< "      7 jumps 8         " 	<< "      11 jumps 12       "	<< endl;
	cout << endl;
	cout << "          ^             " 	<< "          ^             "	<< "          ^             "	<< endl;
	cout << "         /1\\            "	<< "         /1\\            "	<< "         /1\\            "	<< endl;
	cout << "        /   \\           "	<< "        /   \\           "	<< "        /   \\           "	<< endl;
    cout << "       /0   0\\          " << "       /0   0\\          "	<< "       /1   0\\          "	<< endl;
    cout << "      /       \\         " << "      /       \\         "	<< "      /       \\         "	<< endl;
	cout << "     /0   1   1\\        "	<< "     /1   0   0\\        "	<< "     /0   0   0\\        "	<< endl;
	cout << "    /           \\       "	<< "    /           \\       "	<< "    /           \\       "	<< endl;
	cout << "   /1   0   1   1\\      "	<< "   /1   0   1   1\\      "	<< "   /0   0   1   1\\      "	<< endl;
	cout << "  /               \\     "	<< "  /               \\     "	<< "  /               \\     "	<< endl;
	cout << " /0   0   1   1   1\\    "	<< " /0   0   1   1   1\\    "	<< " /0   0   1   1   1\\    "	<< endl;
	cout << "/___________________\\   "	<< "/___________________\\   "	<< "/___________________\\   "	<< endl;
	cout << "      2 jumps 4         "	<< "      6 jumps 5         " 	<< "      7 jumps 4         "	<< endl;
	cout << endl;
	cout << "          ^             " 	<< "          ^             "	<< "          ^             "	<< endl;
	cout << "         /1\\            "	<< "         /0\\            "	<< "         /0\\            "	<< endl;
	cout << "        /   \\           "	<< "        /   \\           "	<< "        /   \\           "	<< endl;
    cout << "       /1   0\\          " << "       /0   0\\          "	<< "       /0   0\\          "	<< endl;
    cout << "      /       \\         " << "      /       \\         "	<< "      /       \\         "	<< endl;
	cout << "     /0   0   0\\        "	<< "     /1   0   0\\        "	<< "     /1   0   0\\        "	<< endl;
	cout << "    /           \\       "	<< "    /           \\       "	<< "    /           \\       "	<< endl;
	cout << "   /0   1   0   0\\      "	<< "   /0   1   0   0\\      "	<< "   /0   1   0   0\\      "	<< endl;
	cout << "  /               \\     "	<< "  /               \\     "	<< "  /               \\     "	<< endl;
	cout << " /0   0   1   1   1\\    "	<< " /0   0   1   1   1\\    "	<< " /0   1   0   0   1\\    "	<< endl;
	cout << "/___________________\\   "	<< "/___________________\\   "	<< "/___________________\\   "	<< endl;
	cout << "      10 jumps 9        "	<< "      1 jumps 4         " 	<< "      14 jumps 13       "	<< endl;
	cout << endl;
	cout << "          ^             " 	<< "          ^             "	<< "          ^             "	<< endl;
	cout << "         /0\\            "	<< "         /0\\            "	<< "         /0\\            "	<< endl;
	cout << "        /   \\           "	<< "        /   \\           "	<< "        /   \\           "	<< endl;
    cout << "       /0   0\\          " << "       /0   0\\          "	<< "       /0   0\\          "	<< endl;
    cout << "      /       \\         " << "      /       \\         "	<< "      /       \\         "	<< endl;
	cout << "     /0   0   0\\        "	<< "     /0   0   0\\        "	<< "     /0   0   0\\        "	<< endl;
	cout << "    /           \\       "	<< "    /           \\       "	<< "    /           \\       "	<< endl;
	cout << "   /0   0   0   0\\      "	<< "   /0   0   0   0\\      "	<< "   /0   0   0   0\\      "	<< endl;
	cout << "  /               \\     "	<< "  /               \\     "	<< "  /               \\     "	<< endl;
	cout << " /0   1   1   0   1\\    "	<< " /0   0   0   1   1\\    "	<< " /0   0   1   0   0\\    "	<< endl;
	cout << "/___________________\\   "	<< "/___________________\\   "	<< "/___________________\\   "	<< endl;
	cout << "      4 jumps 8         "	<< "      12 jumps 13       " 	<< "      15 jumps 14       "	<< endl;
	cout << endl;
	cout << "---------------------------------------------------------------------------------------"	<< endl << endl;
}

void View::showTotalScore(){
	cout << "\nYour cumulative score across all games is: " << totalScore << endl << endl;
}

void View::setTotalScore(int total){
	totalScore = total;
}

int View::getSize(){
	return boardSize;
}


















