#include "Controller.h"
#include <iostream>
using namespace std;

Controller::Controller(){}

Controller::Controller(View v, Model m){
  view = v;
  model = m;
}

Controller::~Controller(){}

void Controller::run(){
	bool again;
	int game;
	int size;
	bool sizeChange;
	bool valid;
	while(true){
		game = view.getGameChoice();
		again = true;
		if(game == 1){ //Peg game
			while(again == true){
				size = view.getSize();
				sizeChange = false;
				model.resize(size);
				switch(size){
					case 0:
						while(!model.checkIfNoMoves()){
							view.setTotalScore(model.getTotalScore());
							view.displayBoard(model.getPegArray(size), size);
							view.getMove();
							if(view.getSize() == 1 || view.getSize() == 2){
								sizeChange = true;
								break;
							}
							valid = model.jumpPegAOverPegB(view.getP1(), view.getP2());
							if(!valid)
								view.invalidMove();
							
						}
						break;
					case 1:
						while(!model.checkIfNoMoves()){
							view.setTotalScore(model.getTotalScore());
							view.displayBoard(model.getPegArray(size), size);
							view.getMove();
							if(view.getSize() == 0 || view.getSize() == 2){
								sizeChange = true;
								break;
							}
							valid = model.jumpPegAOverPegB(view.getP1(), view.getP2());
							if(!valid)
								view.invalidMove();
							
						}
						break;
					case 2:
						while(!model.checkIfNoMoves()){
							view.setTotalScore(model.getTotalScore());
							view.displayBoard(model.getPegArray(size), size);
							view.getMove();
							if(view.getSize() == 0 || view.getSize() == 1){
								sizeChange = true;
								break;
							}
							valid = model.jumpPegAOverPegB(view.getP1(), view.getP2());
							if(!valid)
								view.invalidMove();
							
						}
						break;
				}
				if(sizeChange == false){
					view.displayBoard(model.getPegArray(size), size);
					view.showGameOver(model.getCurrentScore());
					again = view.getPlayAgain();
				}
			}
		}
		else{
			view.notAvailable();
		}
	}
}
