#include <iostream>
#include <string>
using namespace std;

#ifndef VIEW_H
#define VIEW_H
class View{
	public:
		View();
		~View();
		void getMove();
		int getP1();
		int getP2();
		bool validInput();
		void invalidMove();
		void displayMenu();
		int getGameChoice();
		void showGameOver(int score);
		void notAvailable();
		bool getPlayAgain();
		void showHelp();
		void displayBoard(int arr[], int size);
		void showSolution();
		void showTotalScore();
		void setTotalScore(int total);
		int getSize();
	private:
		int p1, p2, gameNum, totalScore, boardSize;
		int* pegArray;
		string p1str, p2str, gamestr;
	
};

#endif