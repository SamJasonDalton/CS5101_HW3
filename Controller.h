#include "Model.h"
#include "View.h"
#include <cstring>

#ifndef CONTROL_H
#define CONTROL_H

class Controller{
  public:
    Controller();
    Controller(View v, Model m);
    ~Controller();
	void run();
  private:
    View view;
    Model model;
};

#endif